<?php declare(strict_types=1);

namespace Test\Plugin\Plugin;


use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Psr\Log\LoggerInterface;

class ProductRepositoryExamplePlugin
{
    /**
     * ProductRepositoryExamplePlugin constructor.
     * @var LoggerInterface
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    public function beforeGetById(ProductRepositoryInterface $subject, $productId, $editMode = false, $storeId = null, $forceReload = false)
    {
        $this->logger->info('Before get product by ID' . $productId);
        return [$productId, $editMode, $storeId, $forceReload];
    }

    public function aroundGetById(ProductRepositoryInterface $subject, callable $proceed, ...$args)
    {
        $result = $proceed(...$args);
        return $result;
    }

    public function afterGetById(ProductRepositoryInterface $subject, ProductInterface $result)
    {
        $this->logger->info('After get product by ID' . $result->getId());
        return $result;
    }
}
