<?php

namespace Test\Tests\Model\ResourceModel\Post;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Post extends AbstractCollection
{
    /**
     * @retrun void
     */

    protected function _construct()
    {
        $this->_init('Test\Tests\Model\Post', 'Test\Tests\Model\ResourceModel\Post');
    }
}
