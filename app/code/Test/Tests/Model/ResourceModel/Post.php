<?php
namespace Test\Tests\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Post extends AbstractDb
{
    /**
     * @retrun void
     */

    protected function _construct()
    {
        $this->_init('test_tests_post', 'post_id');
    }
}
