<?php
namespace Test\Tests\Block;

use Test\Tests\Model\ResourceModel\Post\Colletion as PostCollection;
use Test\Tests\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Test\Tests\Model\Post;


class Posts extends Template
{
    /**
     * @var null|CollectionFactory
     */
    protected $_postCollectionFactory = null;

    /**
     * @param Context $context
     * @param PostCollectionFactory $postCollectionFactory
     * @param array $data
     */
    public function __construct(Context $context, PostCollectionFactory $postCollectionFactory, array $data = [])
    {
        $this->_postCollectionFactory = $postCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return Post[]
     */
    public function getPosts()
    {
        /**
         * @var PostCollection $postCollection
         */
        $postCollection = $this->_postCollectionFactory->create();
        $postCollection->addFieldToSelect('*')->load();
        return $postCollection->getItems();
    }

    /**
     * @param Post $post
     * @return string
     */
    public function getPostUrl(Post $post)
    {
        return '/tests/post/view/id/' . $post->getId();
    }

}
