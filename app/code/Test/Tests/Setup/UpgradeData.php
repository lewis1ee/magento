<?php


namespace Test\Tests\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * Class UpgradeData
 * @package Test\Tests\Setup
 */

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if ($context->getVersion() && version_compare($context->getVersion(), '0.1.2', '<'))
        {
            $tableName = $setup->getTable('test_tests_post');

            $data = [
                [
                    'title' => 'Post 1',
                    'content' => 'content of first post.'
                ],
                [
                    'title' => 'Post 2',
                    'content' => 'content of second post.'
                ]
            ];

            $setup
                ->getConnection()
                ->insertMultiple($tableName, $data);
        }
        $setup->endSetup();
    }
}
