<?php


namespace Tasks\Hobbydigi\Model;
use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;
use Magento\Tests\NamingConvention\true\string;
use \Tasks\HobbyDigi\Api\Data\PostInterface;

/**
 * Class File
 * @package Tasks\Hobbydigi\Model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Answer extends AbstractModel implements PostInterface, IdentityInterface
{
    /**
     * Cache tag
     */
    const CACHE_TAG = 'tasks_hobbydigi_survey';

    /**
     * Post Initialization
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tasks\Hobbydigi\Model\ResourceModel\Post');
    }


    /**
     * Get Title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->getData(self::ANSWER_ID);
    }

    /**
     * Get Content
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getIp()
    {
        return $this->getData(self::CREATED_BY);
    }

    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getAnswerId()];
    }

    /**
     * Set Title
     *
     * @param int $answerId
     * @return $this
     */
    public function setTitle($answerId)
    {
        return $this->setData(self::ANSWER_ID, $answerId);
    }

    /**
     * Set Content
     *
     * @param string $content
     * @return $this
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Set Created At
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Set ID
     *
     * @param string $createdBy
     * @return $this
     */
    public function setIp($createdBy)
    {
        return $this->setData(self::CREATED_BY, $createdBy);
    }
}
