<?php


namespace Tasks\Hobbydigi\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Remittance File Collection Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tasks\Hobbydigi\Model\Post', 'Tasks\Hobbydigi\Model\ResourceModel\Post');
    }
}
