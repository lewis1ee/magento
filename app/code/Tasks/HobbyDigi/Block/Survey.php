<?php


namespace Tasks\Hobbydigi\Block;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Tasks\Hobbydigi\Model\ResourceModel\Collection as AnsCollection;
use \Tasks\Hobbydigi\Model\ResourceModel\CollectionFactory as AnsCollectionFactory;
use \Tasks\Hobbydigi\Model\Answer;

class Survey extends Template
{
    /**
     * CollectionFactory
     * @var null|CollectionFactory
     */
    protected $_ansCollectionFactory = null;

    /**
     * Constructor
     *
     * @param Context $context
     * @param AnsCollectionFactory $ansCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        AnsCollectionFactory $ansCollectionFactory,
        array $data = []
    ) {
        $this->_ansCollectionFactory = $ansCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return Answer[]
     */
    public function getAnswers()
    {
        /** @var AnsCollection $ansCollection */
        $ansCollection = $this->_ansCollectionFactory->create();
        $ansCollection->addFieldToSelect('*')->load();
        return $ansCollection->getItems();
    }
}
