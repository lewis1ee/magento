<?php
namespace Tasks\HobbyDigi\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Result\Page;
use \Magento\Framework\Exception\LocalizedException;

use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;

class Index extends Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    public $remoteAddress;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param RemoteAddress $remoteAddress
     * @codeCoverageIgnore
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        RemoteAddress $remoteAddress
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->remoteAddress = $remoteAddress;
    }

    /**
     * Prints the blog from informed order id
     * @return Page
     * @throws LocalizedException
     */
    public function execute()
    {

        $this->remoteAddress->getRemoteAddress();
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
