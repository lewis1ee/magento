<?php


namespace Tasks\HobbyDigi\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Result\Page;
use \Magento\Framework\Exception\LocalizedException;

use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;


class Save extends action
{

    protected $resultPageFactory;
    private $setup;

    private $remoteAddress;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ModuleDataSetupInterface $setup,
        RemoteAddress $remoteAddress
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->setup = $setup;
        $this->remoteAddress = $remoteAddress;
        parent::__construct($context);
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @return Page
     * @throws LocalizedException
     */

    public function execute()
    {
        $this->setup->startSetup();
        $table = $this->setup->getTable('tasks_hobbydigi_survey_answer');
        $ans = [
            'Content' => $this->getRequest()->getPost('ans'),
            'created_by' => $this->remoteAddress->getRemoteAddress(),
        ];

        if (!empty($ans['Content'])) {
            $this->setup
                ->getConnection()
                ->insert($table, $ans);
        }

        $this->setup->endSetup();

        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
