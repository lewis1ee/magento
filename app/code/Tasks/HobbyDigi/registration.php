<?php

use Magento\Framework\Component\Magento\Framework\Component\ComponentRegistrar;

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Tasks_HobbyDigi',
    __DIR__
);
