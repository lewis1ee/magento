<?php

namespace MyCompany\ExampleAdminNewPage\Api\Data;

interface AnsInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID            = 'answer_id';
    const CONTENT       = 'Content';
    const CREATED_AT    = 'created_at';
    const CREATED_BY    = 'created_by';
    /**#@-*/
    /**
     * Get Title
     *
     * @return int|null
     */
    public function getTitle();

    /**
     * Get Content
     *
     * @return string|null
     */
    public function getContent();

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Get ID
     *
     * @return string|null
     */
    public function getIp();

    /**
     * Set Title
     *
     * @param int $Id
     * @return $this
     */
    public function setTitle($Id);

    /**
     * Set Content
     *
     * @param string $content
     * @return $this
     */
    public function setContent($content);

    /**
     * Set Crated At
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Set ID
     *
     * @param string $ip
     * @return $this
     */
    public function setIp($ip);

}
