<?php

namespace MyCompany\ExampleAdminNewPage\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;
use \MyCompany\ExampleAdminNewPage\Api\Data\AnsInterface;

/**
 * Class File
 * @package MyCompany\ExamplaAdminNewPage\Model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Survey extends AbstractModel implements AnsInterface, IdentityInterface
{
    /**
     * Cache tag
     */
    const CACHE_TAG = 'tasks_hobbydigi_survey_answer';

    /**
     * Post Initialization
     * @return void
     */
    protected function _construct()
    {
        $this->_init('MyCompany\ExampleAdminNewPage\Model\ResourceModel\Survey');
    }


    /**
     * Get Title
     *
     * @return int|null
     */
    public function getTitle()
    {
        return $this->getData(self::ANSWER_ID);
    }

    /**
     * Get Content
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Get ID
     *
     * @return string|null
     */
    public function getIp()
    {
        return $this->getData(self::CREATED_BY);
    }

    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getIp()];
    }

    /**
     * Set Title
     *
     * @param int $Id
     * @return $this
     */
    public function setTitle($Id)
    {
        return $this->setData(self::ANSWER_ID, $Id);
    }

    /**
     * Set Content
     *
     * @param string $content
     * @return $this
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Set Created At
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Set ID
     *
     * @param string $ip
     * @return $this
     */
    public function setIp($ip)
    {
        return $this->setData(self::CREATED_BY, $ip);
    }
}
