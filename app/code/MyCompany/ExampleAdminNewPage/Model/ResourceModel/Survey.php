<?php


namespace MyCompany\ExampleAdminNewPage\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Survey extends AbstractDb
{
    /**
     * Post Abstract Resource Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('tasks_hobbydigi_survey_answer',
            'answer_id');
    }
}
