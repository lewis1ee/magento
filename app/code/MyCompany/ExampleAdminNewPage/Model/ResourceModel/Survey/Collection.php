<?php


namespace MyCompany\ExampleAdminNewPage\Model\ResourceModel\Survey;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('MyCompany\ExampleAdminNewPage\Model\Survey',
            'MyCompany\ExampleAdminNewPage\Model\ResourceModel\Survey');
    }
}
