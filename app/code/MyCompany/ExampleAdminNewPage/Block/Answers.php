<?php
namespace MyCompany\ExampleAdminNewPage\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \MyCompany\ExampleAdminNewPage\Model\ResourceModel\Survey\Collection as AnsCollection;
use \MyCompany\ExampleAdminNewPage\Model\ResourceModel\Survey\CollectionFactory as AnsCollectionFactory;
use \MyCompany\ExampleAdminNewPage\Model\Survey;

class Answers extends Template
{
    /**
     * CollectionFactory
     * @var null|CollectionFactory
     */
    protected $_ansCollectionFactory = null;

    /**
     * Constructor
     *
     * @param Context $context
     * @param AnsCollectionFactory $ansCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        AnsCollectionFactory $ansCollectionFactory,
        array $data = []
    ) {
        $this->_ansCollectionFactory = $ansCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return Survey[]
     */
    public function getAns()
    {
        /** @var AnsCollection $ansCollection */
        $ansCollection = $this->_ansCollectionFactory->create();
        $ansCollection->addFieldToSelect('*')->load();
        return $ansCollection->getItems();
    }

}
