<?php

use Magento\Framework\Component\Magento\Framework\Component\ComponentRegistrar;

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'MyCompany_ExampleAdminNewPage',
    __DIR__
);
