<?php


use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class InitializeDefaultBrands implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;


    public function __construct(ModuleDataSetupInterface $moduleDataSetup)
    {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * @return string[]
     */
    public static function getDependencies()
    {
        return [
            \Magento\Store\Setup\Patch\Schema\InitializeStoresAndWebsites::class
        ];
    }

    /**
     * @return string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return $this
     */
    public function apply()
    {
        $brands = [
            ['name' => 'Sike', 'description' => 'Something cool'],
            ['name' => 'Luma', 'description' => 'Something not quite as cool'],
            ['name' => 'Babidas', 'description' => 'To cool to care']
        ];
        $records = array_map(function ($brand)
        {
            return array_merge($brand, ['is_enabled' => 1, 'website_id' => 1]);
        }, $brands);

        $this->moduleDataSetup->getConnection()->insertMultiple('Table_Brand_Example', $records);

        return $this;
    }
}
